
// MIT, Libre Software 
// Example:   flwatch " iwlist wlx503eaae24f80 scan | grep WLAN1-00 -B2 -A20 " 

// Compilation on BSD and Linux, Devuan:
//   c++  -I"/usr/X11R7/include/" -I"/usr/pkg/include"   -L"/usr/pkg/lib"   -lfltk  flwatch.cxx  -o flwatch

#include <stdio.h>
#include <stdlib.h>
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <math.h>
#include <time.h>
#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Button.H>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <dirent.h>
#include <ctype.h>
#include <sys/stat.h>
#include <dirent.h>
#include <sys/types.h>
#include <unistd.h>  

#include <stdlib.h> 
#include <string.h>
#include <dirent.h>
#include <time.h>
#if defined(__linux__) //linux
#define MYOS 1
#elif defined(_WIN32)
#define MYOS 2
#elif defined(_WIN64)
#define MYOS 3
#elif defined(__unix__) 
#define MYOS 4  // freebsd
#define PATH_MAX 2500
#else
#define MYOS 0
#endif
#include <ctype.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h> 
#include <time.h>

#include <FL/Fl.H>
void loadpanel( const char *pattern );
#include <FL/Fl_Double_Window.H>
extern Fl_Double_Window *win1;
#include <FL/Fl_Button.H>
#include <FL/Fl_Browser.H>
extern Fl_Browser *fbrow;
#include <FL/Fl_Box.H>
Fl_Double_Window* make_window();
int main( int argc, char *argv[]);


Fl_Window     *win = 0;         // main window
//int timercount = 5000;
int timercount = 600;
Fl_Box *box;

char var_user_watchcmd[PATH_MAX];
int flfontsize = 14;  

Fl_Browser *fbrow=(Fl_Browser *)0;


void cb_Update(void*) 
{
    //box->update();
    printf( "Time\n" );
    char charo[2500];
    printf( "%d\n", (int)time(NULL));
    snprintf( charo , sizeof(charo ), " Your time : %d",  (int)time(NULL) );

    fbrow->clear( );    
    fbrow->add( " = CONTENT = " ); 

    FILE *fp1; 
    char linereadtmp[PATH_MAX];
    char lineread[PATH_MAX];
    fp1 = popen( var_user_watchcmd , "r");
    while( !feof(fp1) ) 
    {
             fgets( linereadtmp, PATH_MAX, fp1); 
             //strncpy( lineread, strrlf(linereadtmp) , PATH_MAX );
	     strncpy( lineread, linereadtmp , PATH_MAX ); 
             if ( !feof(fp1) )  
	     {
	        //printf( "%s\n" , lineread );
                fbrow->add( lineread ); 
	     }
    }
    pclose( fp1 );

    box->label( charo );
    win->redraw();
    Fl::repeat_timeout(2.00, cb_Update);
}






int main(int argc, char *argv[])
{

	if ( argc == 2 )
	if ( strcmp( argv[1] , "-t" ) ==  0 ) 
	{
			printf("%d\n", (int)time(NULL));
			return 0;
	}

        printf( " Example:   flwatch ' iwlist wlx503eaae24f80 scan | grep WLAN1-00 -B2 -A20 '  \n" ); 

	if ( argc == 1 )
	{
	   printf( "No Argument found.\n" ); 
	   return 0; 
	}



    if ( argc >= 2 )
       strncpy(  var_user_watchcmd, argv[ 1 ], PATH_MAX );

    win = new Fl_Window( 800, 600, "FLWATCH");
    box = new Fl_Box( 10, 10, 160 , 40 ,"Hello World" );

    fbrow = new Fl_Browser( 20, 60 , 800 -20 - 20 , 600 - 60 - 20  );
    fbrow->labelsize( flfontsize  );
    fbrow->textsize( flfontsize  );
    fbrow->type(FL_HOLD_BROWSER);

    win->resizable(win);
    win->show();
    Fl::add_timeout(2.0, cb_Update);
    return(Fl::run());
}



